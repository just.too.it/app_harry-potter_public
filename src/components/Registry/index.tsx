import React, { FC } from 'react';

import { HeroPreview } from '../../core/components/Hero/HeroPreview';

import { Hero } from '../../core/components/Hero/hero.type';

export const Registry: FC<{ data: Hero[] | null; count: number; activePage: number }> = (props) => {
  const start = (props.activePage - 1) * props.count;
  const end = start + props.count;

  const cardsOnDisplay = props.data?.slice(start, end);

  return (
    <ul className="heroes__list">
      {cardsOnDisplay?.map((card) => (
        <li key={card.id}>
          <HeroPreview hero={card} />
        </li>
      ))}
    </ul>
  );
};
