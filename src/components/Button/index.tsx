import React, { FC } from 'react';

import { IButton } from './button.type';

export const Button: FC<{ btn: IButton }> = (props) => {
  let btnClassName = 'button';
  btnClassName += props.btn.className ? props.btn.className : '';
  btnClassName += props.btn.size === 'large' ? ' button_large' : ' button_small';

  if (props.btn.size === 'small') {
    btnClassName += props.btn.isGreen ? ' button_small-green' : ' button_small-beige';
  }

  const btnID = props.btn.id ? props.btn.id : '';

  return (
    <button className={btnClassName} id={btnID} type={props.btn.type} onClick={props.btn.btnOnClick}>
      {props.children}
    </button>
  );
};
