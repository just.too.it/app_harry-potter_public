export interface IButton {
  type: 'button' | 'submit' | 'reset';
  text?: string;
  id?: string;
  size?: 'large' | 'small';
  className?: string;
  isGreen?: boolean;
  btnOnClick(): void;
}
