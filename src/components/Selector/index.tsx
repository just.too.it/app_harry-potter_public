import React, { FC, useState, useEffect, useRef } from 'react';

import { heroesActions } from '../../store/heroes/heroesSlice';
import { useAppDispatch, useAppSelector } from '../../store/store.hooks';

import { OptionsForSelector } from './optionsForSelector.type';

export const Selector: FC<{ header: string; options: OptionsForSelector[]; selectorType: string[] }> = (props) => {
  const dispatch = useAppDispatch();
  const heroesState = useAppSelector((state) => state.heroesReducer);

  const [list, setListOpen] = useState(false);
  const [header, setHeader] = useState(props.header);

  const listClassName = list ? 'selector__list selector__list_opened' : 'selector__list';

  const toggleList = (): void => {
    setListOpen(!list);
  };

  const listRef = React.useRef() as React.MutableRefObject<HTMLUListElement>;

  useEffect(() => {
    const handler = (event) => {
      if (listRef.current && !listRef.current.contains(event.target) && event.target.className !== 'selector__header') {
        setListOpen(false);
      }
    };

    document.addEventListener('mousedown', handler);

    return () => {
      document.removeEventListener('mousedown', handler);
    };
  }, [list]);

  useEffect(() => {
    props.selectorType.length !== 0 ? setHeader(`Выбрано: ${props.selectorType.length}`) : setHeader(props.header);
  }, [props.selectorType]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const currentIndex = props.selectorType.indexOf(e.target.id);
    const filteredItems = [...props.selectorType];

    if (currentIndex === -1) {
      filteredItems.push(e.target.id);
    } else {
      filteredItems.splice(currentIndex, 1);
    }

    switch (props.selectorType) {
      case heroesState.filtersByGender:
        dispatch(heroesActions.setFiltersGender(filteredItems));
        break;
      case heroesState.filtersBySide:
        dispatch(heroesActions.setFiltersSide(filteredItems));
        break;
      case heroesState.filtersByRace:
        dispatch(heroesActions.setFiltersRace(filteredItems));
        break;
    }
  };

  return (
    <div className="selector">
      {
        <div className="selector__header" onClick={toggleList}>
          {header}
        </div>
      }
      {
        <ul className={listClassName} ref={listRef}>
          {props.options.map((opt) => (
            <li className="selector__item" key={opt.id}>
              <input
                type="checkbox"
                className="selector__checkbox"
                id={opt.id}
                value={opt.value}
                onChange={handleChange}
                checked={props.selectorType.includes(opt.id) ? true : false}
              />
              <label htmlFor={opt.id} className="selector__label">
                {opt.value}
              </label>
            </li>
          ))}
        </ul>
      }
    </div>
  );
};
