export interface OptionsForSelector {
  id: string;
  value: string;
}
