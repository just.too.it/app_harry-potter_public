import { Field, ErrorMessage } from 'formik';
import React from 'react';

export const InputFormik = (props): React.ReactElement => {
  const { label, name, wrapperClassName, ...rest } = props;

  return (
    <div className={wrapperClassName}>
      <label htmlFor={name} className="form__label">
        {label}
      </label>
      <Field id={name} name={name} className="form__input" {...rest} />
      {ErrorMessage.name ? (
        <div className={'form__error'}>
          <ErrorMessage name={name} />
        </div>
      ) : null}
    </div>
  );
};
