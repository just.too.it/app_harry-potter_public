import React from 'react';

export const Input = (props): React.ReactElement => {
  const { type, name, className, placeholder, value, onChange } = props;
  return (
    <input type={type} name={name} className={className} placeholder={placeholder} value={value} onChange={onChange} />
  );
};
