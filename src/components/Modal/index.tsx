import React, { FC } from 'react';

import { useAppDispatch } from '../../store/store.hooks';
import { ButtonIcon } from '../ButtonIcon';
import { IButtonIcon } from '../ButtonIcon/buttonIcon.type';

export const Modal: FC<{ active; setActive; children }> = (props) => {
  const dispatch = useAppDispatch();

  const btnClose: IButtonIcon = {
    type: 'button',
    className: 'modal__close',
    btnOnClick: () => {
      dispatch(props.setActive(false));
    },
  };

  return (
    <div
      className={props.active ? 'modal modal_opened' : 'modal'}
      onClick={() => {
        dispatch(props.setActive(false));
      }}
    >
      <div className="modal__content" onClick={(e) => e.stopPropagation()}>
        {props.children}
        <ButtonIcon btn={btnClose} />
      </div>
    </div>
  );
};
