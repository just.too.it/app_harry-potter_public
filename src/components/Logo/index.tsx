import React from 'react';

import logo from './img/logo.png';

export const Logo = (): React.ReactElement => {
  return <img src={logo} className="logo" alt="Логотип Harry Potter" />;
};
