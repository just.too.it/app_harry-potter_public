import { Field, ErrorMessage } from 'formik';
import React from 'react';

export const Textarea = (props): React.ReactElement => {
  const { label, name, count, ...rest } = props;
  return (
    <div>
      <div className="form__label_plus">
        <label htmlFor={name} className="form__label">
          {label}
        </label>
        <input type="text" className="form__count" value={count} disabled />
      </div>
      <Field as="textarea" id={name} name={name} className="form__input form__input_text" maxLength="100" {...rest} />
      {ErrorMessage.name ? (
        <div className={'form__error'}>
          <ErrorMessage name={name} />
        </div>
      ) : null}
    </div>
  );
};
