import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';

import { LinkForNav } from './linkForNav.type';

export const Nav: FC<{ items: LinkForNav[] }> = (props) => {
  return (
    <nav>
      <ul className="nav__list">
        {props.items.map((item) => (
          <li className="nav__item" key={item.id}>
            <NavLink to={item.href} exact={true} className="nav__link" activeClassName="nav__link_current">
              {item.label}
            </NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
};
