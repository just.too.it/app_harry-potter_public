export interface LinkForNav {
  id: number;
  href: string;
  label: string;
}
