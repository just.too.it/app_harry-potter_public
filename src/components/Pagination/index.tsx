import React from 'react';

import { heroesActions } from '../../store/heroes/heroesSlice';
import { useAppDispatch } from '../../store/store.hooks';
import { ButtonIcon } from '../ButtonIcon';
import { IButtonIcon } from '../ButtonIcon/buttonIcon.type';

export const Pagination = ({ countPages, page }): React.ReactElement => {
  const dispatch = useAppDispatch();

  const btnPrev: IButtonIcon = {
    type: 'button',
    id: 'prev',
    className: 'pagination__button pagination__button_previous',
    btnOnClick: () => {
      if (page > 1) {
        dispatch(heroesActions.setPage(--page));
      }
    },
  };

  const btnNext: IButtonIcon = {
    type: 'button',
    id: 'next',
    className: 'pagination__button pagination__button_next',
    btnOnClick: () => {
      if (page < countPages) {
        dispatch(heroesActions.setPage(++page));
      } else {
        page = countPages;
      }
    },
  };

  interface Point {
    id: number;
  }
  const points: Point[] = [];
  for (let i = 1; i <= countPages; i++) {
    points.push({ id: i });
  }

  let start: number;
  let end: number;
  const countPoints = 3;

  if (page === 1) {
    start = 0;
    end = countPoints;
  } else if (page === countPages) {
    start = countPages - countPoints;
    start = start < 0 ? 0 : start;
    end = countPages;
  } else {
    start = page - 2;
    end = Number(page) + 1;
  }

  const pointsOnDisplay = points.slice(start, end);
  
  return (
    <ul className="pagination">
      <li>
        <ButtonIcon btn={btnPrev} />
      </li>
      <li>
        <ul className="pagination__list">
          {pointsOnDisplay.map((point) => (
            <li key={point.id}>
              <input
                type="radio"
                name="radio"
                data-id={point.id}
                className={page === point.id ? 'pagination__item pagination__item_current' : 'pagination__item'}
                onChange={() => dispatch(heroesActions.setPage(point.id))}
              />
            </li>
          ))}
        </ul>
      </li>
      <li>
        <ButtonIcon btn={btnNext} />
      </li>
    </ul>
  );
};
