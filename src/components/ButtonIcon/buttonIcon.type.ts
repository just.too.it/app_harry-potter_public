export interface IButtonIcon {
  type: 'button' | 'submit' | 'reset';
  id?: string;
  className: string;
  btnOnClick(): void;
}
