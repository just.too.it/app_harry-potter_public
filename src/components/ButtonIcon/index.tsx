import React, { FC } from 'react';

import { IButtonIcon } from './buttonIcon.type';

export const ButtonIcon: FC<{ btn: IButtonIcon }> = (props) => {
  const { type, id, className, btnOnClick } = props.btn;

  const btnID = id ? id : '';

  return <button className={className} id={btnID} type={type} onClick={btnOnClick}></button>;
};
