import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { OptionsForSelector } from '../../components/Selector/optionsForSelector.type';
import { Hero } from '../../core/components/Hero/hero.type';
import { initialHeroes } from '../../core/utils/initialHeroes';

interface HeroesState {
  currentPage: number;
  registry: Hero[] | null;
  query: string;
  filtersByGender: string[];
  filtersByRace: string[];
  filtersBySide: string[];
  isHeroModalOpen: boolean;
  displayHero: Hero | null;
  isFormOpen: boolean;
  optionsForGender: OptionsForSelector[];
  optionsForRace: OptionsForSelector[];
  optionsForSide: OptionsForSelector[];
}

const initialState: HeroesState = {
  currentPage: initialHeroes.current,
  registry: null,
  query: '',
  filtersByGender: [],
  filtersByRace: [],
  filtersBySide: [],
  isHeroModalOpen: false,
  displayHero: null,
  isFormOpen: false,
  optionsForGender: [],
  optionsForRace: [],
  optionsForSide: [],
};

export const heroesSlice = createSlice({
  name: 'heroes',
  initialState,
  reducers: {
    setPage: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },
    setRegistry: (state, action: PayloadAction<Hero[]>) => {
      state.registry = action.payload;
    },
    setQuery: (state, action: PayloadAction<string>) => {
      state.query = action.payload;
    },
    setFiltersGender: (state, action: PayloadAction<string[]>) => {
      state.filtersByGender = action.payload;
    },
    setFiltersRace: (state, action: PayloadAction<string[]>) => {
      state.filtersByRace = action.payload;
    },
    setFiltersSide: (state, action: PayloadAction<string[]>) => {
      state.filtersBySide = action.payload;
    },
    setIsHeroModalOpen: (state, action: PayloadAction<boolean>) => {
      state.isHeroModalOpen = action.payload;
    },
    setDisplayHero: (state, action: PayloadAction<Hero>) => {
      state.displayHero = action.payload;
    },
    setIsFormOpen: (state, action: PayloadAction<boolean>) => {
      state.isFormOpen = action.payload;
    },
    setOptionsForGender: (state, action: PayloadAction<OptionsForSelector[]>) => {
      state.optionsForGender = action.payload;
    },
    setOptionsForRace: (state, action: PayloadAction<OptionsForSelector[]>) => {
      state.optionsForRace = action.payload;
    },
    setOptionsForSide: (state, action: PayloadAction<OptionsForSelector[]>) => {
      state.optionsForSide = action.payload;
    },
  },
});

export const heroesReducer = heroesSlice.reducer;
export const heroesActions = heroesSlice.actions;
