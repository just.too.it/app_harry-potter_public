import ReactDOM from 'react-dom';

import './scss/styles.scss';
import { Provider } from 'react-redux';

import { App } from './app';
import { setupStore } from './store/store';

const store = setupStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
