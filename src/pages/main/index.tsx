import React, { FC } from 'react';

import { useHistory } from 'react-router-dom';

import { Button } from '../../components/Button';
import { IButton } from '../../components/Button/button.type';
import { HEROES_PAGE } from '../../core/constants/links';

export const MainPage: FC = (): React.ReactElement => {
  const history = useHistory();

  const buttonBegin: IButton = {
    type: 'button',
    id: 'begin',
    size: 'large',
    btnOnClick: () => {
      history.push(HEROES_PAGE);
    },
  };

  return (
    <div>
      <section className="main">
        <h1 className="main__title">
          Найди любимого персонажа
          <br /> “Гарри Поттера”
        </h1>
        <p className="main__text">Вы сможете узнать тип героев, их способности, сильные стороны и недостатки.</p>
        <Button btn={buttonBegin}>Начать</Button>
      </section>
    </div>
  );
};
