import React, { FC } from 'react';

import { useHistory } from 'react-router-dom';

import { Button } from '../../components/Button';
import { MAIN_PAGE } from '../../core/constants/links';

export const Error: FC = (): React.ReactElement => {
  const history = useHistory();

  return (
    <div>
      <section className="error">
        <h1 className="error__title">404</h1>
        <p className="error__text">Ошибка 404. Такая страница не существует либо она была удалена.</p>
        <Button
          btn={{
            type: 'button',
            id: 'begin',
            size: 'large',
            btnOnClick: () => {
              history.push(MAIN_PAGE);
            },
          }}
        >
          На главную
        </Button>
      </section>
    </div>
  );
};
