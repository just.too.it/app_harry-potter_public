import qs from 'querystring';

import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { Pagination } from '../../components/Pagination';
import { Registry } from '../../components/Registry';
import { AddForm } from '../../core/components/AddForm';
import { Filtering } from '../../core/components/Filtering';
import { ModalWithHero } from '../../core/components/ModalWithHero';
import { HEROES_PAGE } from '../../core/constants/links';
import { heroFactory, heroFullFactory } from '../../core/hero.factory';
import { getGenderOptions, getRaceOptions, getSideOptions } from '../../core/services/dictionary/dictionary.service';
import { getHeroes, getHero } from '../../core/services/hero/hero.service';
import { HeroFullModel } from '../../core/services/hero/heroFull.model';
import { HeroPreviewModel } from '../../core/services/hero/heroPreview.model';
import { initialHeroes } from '../../core/utils/initialHeroes';
import { getPageCount } from '../../core/utils/utils';
import { heroesActions } from '../../store/heroes/heroesSlice';
import { useAppDispatch, useAppSelector } from '../../store/store.hooks';

export const HeroesPage = (): React.ReactElement => {
  const dispatch = useAppDispatch();
  const heroesState = useAppSelector((state) => state.heroesReducer);
  const history = useHistory();
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    const search = history.location.search.substr(1);
    const parsed = qs.parse(search);

    void getGenderOptions.then((data) => {
      dispatch(heroesActions.setOptionsForGender(data));
      return null;
    });
    void getRaceOptions.then((data) => {
      dispatch(heroesActions.setOptionsForRace(data));
      return null;
    });
    void getSideOptions.then((data) => {
      dispatch(heroesActions.setOptionsForSide(data));
      return null;
    });

    void getHeroes()
      .then((data) => (data as HeroPreviewModel[]).map((i) => heroFactory(i)))
      .then((data) => {
        dispatch(heroesActions.setRegistry(data));
        return null;
      });

    if (parsed.page) {
      dispatch(heroesActions.setPage(Number(parsed.page)));
    }

    if (parsed.values) {
      dispatch(heroesActions.setQuery(parsed.values.toString()));
    }

    if (parsed.gender) {
      dispatch(heroesActions.setFiltersGender(parsed.gender.toString().split(',')));
    }
    if (parsed.race) {
      dispatch(heroesActions.setFiltersRace(parsed.race.toString().split(',')));
    }
    if (parsed.side) {
      dispatch(heroesActions.setFiltersSide(parsed.side.toString().split(',')));
    }

    const id = history.location.pathname.substr(8);
    if (id.length === 36) {
      dispatch(heroesActions.setIsHeroModalOpen(true));
      void getHero(id)
        .then((data) => heroFullFactory(data as HeroFullModel))
        .then((data) => dispatch(heroesActions.setDisplayHero(data)));
    }

    if (history.location.pathname.substr(8) === 'new') {
      dispatch(heroesActions.setIsFormOpen(true));
    }
  }, []);

  useEffect(() => {
    setTotalPages(
      getPageCount(
        heroesState.registry ? heroesState.registry.length : 0,
        initialHeroes.countItems,
        initialHeroes.totalItems
      )
    );
  }, [heroesState.registry]);

  useEffect(() => {
    history.push({
      pathname: HEROES_PAGE,
      search: `?values=${heroesState.query}&gender=${heroesState.filtersByGender.join(
        ','
      )}&race=${heroesState.filtersByRace.join(',')}&side=${heroesState.filtersBySide.join(',')}&page=${
        heroesState.currentPage
      }`,
    });
    if (heroesState.registry !== null && heroesState.registry !== undefined) {
      if (totalPages < heroesState.currentPage) {
        dispatch(heroesActions.setPage(1));
      }
    }
  }, [
    heroesState.query,
    heroesState.currentPage,
    heroesState.filtersByGender,
    heroesState.filtersByRace,
    heroesState.filtersBySide,
    totalPages,
  ]);

  return (
    <div className="heroes">
      <div className="heroes__container">
        <Filtering />
        <Registry
          data={heroesState.registry ? heroesState.registry : null}
          count={initialHeroes.countItems}
          activePage={heroesState.currentPage}
        />
        {heroesState.displayHero !== null ? <ModalWithHero hero={heroesState.displayHero} /> : null}
        <AddForm />
        <Pagination countPages={totalPages} page={heroesState.currentPage} />
      </div>
    </div>
  );
};
