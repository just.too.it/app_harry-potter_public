import React, { FC } from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Header } from './core/components/Header';

import AppRouter from './routes';

export const App: FC = (): React.ReactElement => {
  return (
    <BrowserRouter>
      <Header />
      <AppRouter />
    </BrowserRouter>
  );
};
