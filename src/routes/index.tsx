import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { MAIN_PAGE, HEROES_PAGE, HEROES_PAGE_ADD } from '../core/constants/links';
import { Error } from '../pages/error';
import { HeroesPage } from '../pages/heroes';
import { MainPage } from '../pages/main';

const AppRouter = () => {
  return (
    <Switch>
      <Route exact path={MAIN_PAGE}>
        <MainPage />
      </Route>
      <Route path={HEROES_PAGE_ADD} exact>
        <HeroesPage />
      </Route>
      <Route path={`${HEROES_PAGE}/:id`} exact>
        <HeroesPage />
      </Route>
      <Route path={HEROES_PAGE} exact>
        <HeroesPage />
      </Route>
      {/* <Redirect to={'/'} /> */}
      <Route>
        <Error />
      </Route>
    </Switch>
  );
};

export default AppRouter;
