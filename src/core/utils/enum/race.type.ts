export enum Race {
  Human = 'Человек',
  HalfHuman = 'Получеловек',
  NotDefined = 'Неопределен',
}
