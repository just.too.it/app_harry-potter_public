export enum Gender {
  Man = 'Мужчина',
  Woman = 'Женщина',
  NotDefined = 'Неопределен',
}
