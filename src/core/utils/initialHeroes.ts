export interface InitialData {
  countItems: number;
  totalItems: number;
  current: number;
}

export const initialHeroes: InitialData = {
  countItems: 3,
  totalItems: 15,
  current: 1,
};
