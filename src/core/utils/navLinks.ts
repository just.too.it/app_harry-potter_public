import { LinkForNav } from '../../components/Nav/linkForNav.type';
import { HEROES_PAGE, MAIN_PAGE } from '../constants/links';

export const navLinks: LinkForNav[] = [
  {
    id: 1,
    href: MAIN_PAGE,
    label: 'Главная',
  },
  {
    id: 2,
    href: HEROES_PAGE,
    label: 'Персонажи',
  },
];
