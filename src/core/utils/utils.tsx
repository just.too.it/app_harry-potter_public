export const getPageCount = (totalCount: number, limitOnPage: number, totalLimit: number): number => {
  if (totalLimit < totalCount) {
    totalCount = totalLimit;
  }
  return Math.ceil(totalCount / limitOnPage);
};

export function getCrossArray<T>(arr1: T[], arr2: T[], arr3: T[]): T[] {
  return arr1.filter((i) => arr2.includes(i)).filter((e) => arr3.includes(e));
}

export const deleteLastСomma = (str: string): string => {
  if (str.slice(-1) === ',') {
    str = str.slice(0, -1);
  }
  return str;
};
