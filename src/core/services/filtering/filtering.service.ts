import { deleteLastСomma } from '../../utils/utils';

import { HeroPreviewModel } from '../hero/heroPreview.model';

export const getFilters = async (query: string, genders: string[], races: string[], sides: string[]) => {
  let genderQuery = '';
  genders.map((item) => (genderQuery += `${item},`));
  genderQuery = deleteLastСomma(genderQuery);

  let raceQuery = '';
  races.map((item) => (raceQuery += `${item},`));
  raceQuery = deleteLastСomma(raceQuery);

  let sideQuery = '';
  sides.map((item) => (sideQuery += `${item},`));
  sideQuery = deleteLastСomma(sideQuery);

  const url = `http://localhost:5000/api/HARRY_POTTER/character?values=${query}&gender=${genderQuery}&race=${raceQuery}&side=${sideQuery}`;
  const response = await fetch(url);
  try {
    const json = await response.json();
    return json.content as HeroPreviewModel[];
  } catch (error) {
    alert(error);
  }
};
