export interface HeroFullModel {
  id: string;
  name: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  description: string;
  gender: {
    id: string;
    value: string;
  };
  race: {
    id: string;
    value: string;
  };
  side: {
    id: string;
    value: string;
  };
  tag1: string;
  tag2: string;
  tag3: string;
}
