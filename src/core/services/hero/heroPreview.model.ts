export interface HeroPreviewModel {
  id: string;
  name: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  gender: string;
  race: string;
  side: string;
}
