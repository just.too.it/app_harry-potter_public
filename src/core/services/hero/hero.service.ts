import { HeroFullModel } from './heroFull.model';
import { HeroPreviewModel } from './heroPreview.model';

export const getHeroes = async (): Promise<HeroPreviewModel[] | undefined> => {
  const url = 'http://localhost:5000/api/HARRY_POTTER/character';
  const response = await fetch(url);
  try {
    const parseResponse = await response.json();
    return parseResponse.content as HeroPreviewModel[];
  } catch (error) {
    alert(error);
  }
};

export const getHero = async (id): Promise<HeroFullModel | undefined> => {
  const url = `http://localhost:5000/api/HARRY_POTTER/character/${id}`;
  const response = await fetch(url);
  try {
    const parseResponse = await response.json();
    return parseResponse as HeroFullModel;
  } catch (error) {
    alert('Персонажа нет в базе данных');
  }
};

export const postHero = async (hero: HeroFullModel) => {
  const url = `http://localhost:5000/api/HARRY_POTTER/character`;
  try {
    await fetch(url, {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(hero),
    });
  } catch (error) {
    alert(error);
  }
};
