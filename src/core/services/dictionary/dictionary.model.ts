export interface DictionaryModel {
  id: string;
  value: string;
}
