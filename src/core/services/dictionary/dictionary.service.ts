import { dictionaryFactory } from '../../dictionary.factory';

import { DictionaryModel } from './dictionary.model';

export const getOptionsForSelector = async (selector: string): Promise<DictionaryModel[] | undefined> => {
  const url = `http://localhost:5000/api/HARRY_POTTER/${selector}`;
  const response = await fetch(url);
  try {
    const parseResponse = await response.json();
    return parseResponse as DictionaryModel[];
  } catch (error) {
    alert(error);
  }
};

export const getGenderOptions = getOptionsForSelector('gender').then((data) =>
  (data as DictionaryModel[]).map((i) => dictionaryFactory(i))
);
export const getRaceOptions = getOptionsForSelector('race').then((data) =>
  (data as DictionaryModel[]).map((i) => dictionaryFactory(i))
);
export const getSideOptions = getOptionsForSelector('side').then((data) =>
  (data as DictionaryModel[]).map((i) => dictionaryFactory(i))
);
