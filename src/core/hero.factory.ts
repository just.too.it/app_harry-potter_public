import { HeroFromForm } from './components/AddForm/heroFromForm.type';
import { Hero } from './components/Hero/hero.type';

import { HeroFullModel } from './services/hero/heroFull.model';
import { HeroPreviewModel } from './services/hero/heroPreview.model';

export const heroFactory = (model: HeroPreviewModel): Hero => {
  return {
    id: model.id,
    name: model.name,
    surname: '',
    gender: {
      id: '',
      value: model.gender,
    },
    race: {
      id: '',
      value: model.race,
    },
    side: {
      id: '',
      value: model.side,
    },
    colorName: model.nameColor,
    colorBackground: model.backgroundColor,
    colorPropertys: model.parametersColor,
    img: model.imageURL,
    imgModal: '',
    text: '',
    tag1: '',
    tag2: '',
    tag3: '',
  };
};

export const heroFullFactory = (model: HeroFullModel): Hero => {
  return {
    id: model.id,
    name: model.name,
    surname: '',
    gender: {
      get id(): string {
        return model.gender !== null ? model.gender.id : '';
      },
      get value(): string {
        return model.gender !== null ? model.gender.value : '';
      },
    },
    race: {
      get id(): string {
        return model.race !== null ? model.race.id : '';
      },
      get value(): string {
        return model.race !== null ? model.race.value : '';
      },
    },
    side: {
      get id(): string {
        return model.side !== null ? model.side.id : '';
      },
      get value(): string {
        return model.side !== null ? model.side.value : '';
      },
    },
    colorName: model.nameColor,
    colorBackground: model.backgroundColor,
    colorPropertys: model.parametersColor,
    img: model.imageURL,
    imgModal: '',
    text: model.description,
    tag1: model.tag1,
    tag2: model.tag2,
    tag3: model.tag3,
  };
};

export const heroModelFactory = (hero: HeroFromForm): HeroFullModel => {
  return {
    id: '',
    name: hero.name,
    description: hero.description,
    imageURL: hero.img,
    nameColor: hero.colorName,
    backgroundColor: hero.colorBackground,
    parametersColor: hero.colorPropertys,

    gender: {
      get id(): string {
        if (
          this.value.toLowerCase() === 'man' ||
          this.value.toLowerCase() === 'мужчина' ||
          this.value.toLowerCase() === 'мужской'
        ) {
          return 'ad363ad1-9879-45d7-9a01-c1e28955194b';
        } else if (
          this.value.toLowerCase() === 'woman' ||
          this.value.toLowerCase() === 'женщина' ||
          this.value.toLowerCase() === 'женский'
        ) {
          return '18014503-9aae-47eb-abc7-b2ca7c595eb0';
        } else {
          return '6de244eb-591b-4fbe-a023-3f33bd45db21';
        }
      },
      value: hero.gender,
    },
    race: {
      get id(): string {
        if (this.value.toLowerCase() === 'human' || this.value.toLowerCase() === 'человек') {
          return 'a9c7683f-9eca-421b-83a3-6e800c8d59bc';
        } else if (this.value.toLowerCase() === 'halfhuman' || this.value.toLowerCase() === 'получеловек') {
          return 'bd715a7a-4b3a-45d5-98a4-721cd61ba2d8';
        } else {
          return '5b2bcf2d-6560-4320-9603-70ad5785e8cf';
        }
      },
      value: hero.race,
    },
    side: {
      get id(): string {
        if (this.value.toLowerCase() === 'good' || this.value.toLowerCase() === 'добро') {
          return 'f07aeb0c-ef58-44a9-b185-daddb3a3a340';
        } else if (this.value.toLowerCase() === 'evil' || this.value.toLowerCase() === 'зло') {
          return 'ad363ad1-9879-45d7-9a01-c1e28955194b'; // id в базе для "зла" совпадает с "мужчина"
        } else {
          return 'aa26c7e3-02a1-4697-8b27-fb24ae1739dc';
        }
      },
      value: hero.side,
    },
    tag1: hero.tags.split(',')[0],
    tag2: hero.tags.split(',')[1],
    tag3: hero.tags.split(',')[2],
  };
};
