import { OptionsForSelector } from '../components/Selector/optionsForSelector.type';

import { DictionaryModel } from './services/dictionary/dictionary.model';

export const dictionaryFactory = (model: DictionaryModel): OptionsForSelector => {
  return {
    id: model.id,
    value: model.value,
  };
};
