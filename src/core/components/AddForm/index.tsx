import { Formik, Form } from 'formik';
import React from 'react';

import * as yup from 'yup';

import { Button } from '../../../components/Button';
import { InputFormik } from '../../../components/Input/InputFormik';
import { Modal } from '../../../components/Modal';
import { Textarea } from '../../../components/Textarea/Textarea';
import { heroesActions } from '../../../store/heroes/heroesSlice';
import { useAppDispatch, useAppSelector } from '../../../store/store.hooks';
import { SliderForForm } from '../SliderForForm';
import { addClassesForHero } from '../SliderForForm/addClassesForHero';

import { addHero } from './addHero';
import { heroFormFactory } from './heroForm.factory';

export const AddForm = (): React.ReactElement => {
  const dispatch = useAppDispatch();
  const heroesState = useAppSelector((state) => state.heroesReducer);
  const formClassName = heroesState.isFormOpen ? 'form form_opened' : 'form';
  heroesState.isFormOpen ? addClassesForHero() : null;

  const initialValues = {
    name: '',
    gender: '',
    race: '',
    side: '',
    description: '',
    tags: '',
    colorName: '#FFFFFF',
    colorBackground: '#462929',
    colorPropertys: '#FFFFFF',
    img: '',
  };

  const validationSchema = yup.object().shape({
    name: yup.string().required('Обязательно для заполнения'),
    description: yup.string().max(100, 'Длина описания не должна превышать 100 символов!'),
    tags: yup.string().test('tags', 'Количество тегов должно быть равно 3м', (val) => val?.split(',').length === 3),
  });

  return (
    <Modal active={heroesState.isFormOpen} setActive={heroesActions.setIsFormOpen}>
      <Formik
        initialValues={initialValues}
        validateOnBlur
        onSubmit={(values, { resetForm }) => {
          addHero(values);
          dispatch(heroesActions.setIsFormOpen(false));
          resetForm();
        }}
        validationSchema={validationSchema}
      >
        {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isValid, dirty }) => (
          <Form className={formClassName}>
            <div className="form__add">
              <InputFormik label="Добавить имя" name="name" />
              <ul className="form__filters">
                <li>
                  <InputFormik label="Пол" name="gender" />
                </li>
                <li>
                  <InputFormik label="Раса" name="race" />
                </li>
                <li>
                  <InputFormik label="Сторона" name="side" />
                </li>
              </ul>
              <Textarea label="Добавить описание" name="description" count={`${values.description.length}/100`} />
              <Textarea
                label="Добавить теги"
                name="tags"
                count={`${values.tags.split(',').length}/3`}
                className="form__input form__input_tags"
              />
              <div className="form__images">
                <div className="form__photo">
                  <label className="form__label">Добавить фото</label>
                  <img src={values.img} width="188px" height="210px" className="form__img" />
                  <InputFormik label="" name="img" placeholder="URL изображения" />
                </div>
                <div className="form__colors">
                  Выбрать цвет
                  <ul>
                    <li>
                      <InputFormik
                        type="color"
                        label="Цвет имени"
                        name="colorName"
                        className="form__сolor"
                        wrapperClassName="form__field"
                      />
                    </li>
                    <li>
                      <InputFormik
                        type="color"
                        label="Цвет фона параметров"
                        name="colorBackground"
                        className="form__сolor"
                        wrapperClassName="form__field"
                      />
                    </li>
                    <li>
                      <InputFormik
                        type="color"
                        label="Цвет параметров"
                        name="colorPropertys"
                        className="form__сolor"
                        wrapperClassName="form__field"
                      />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <SliderForForm hero={heroFormFactory(values)} />
            <div className="form__button">
              <Button btn={{ type: 'submit', isGreen: true, btnOnClick: handleSubmit }}>Сохранить</Button>
            </div>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};
