import { Hero } from '../Hero/hero.type';

import { HeroFromForm } from './heroFromForm.type';

export const heroFormFactory = (hero: HeroFromForm): Hero => {
  return {
    id: '',
    name: hero.name,
    surname: '',
    gender: {
      id: '',
      value: hero.gender,
    },
    race: {
      id: '',
      value: hero.race,
    },
    side: {
      id: '',
      value: hero.side,
    },
    colorName: hero.colorName,
    colorBackground: hero.colorBackground,
    colorPropertys: hero.colorPropertys,
    img: hero.img,
    imgModal: '',
    text: hero.description,
    tag1: hero.tags.split(',')[0],
    tag2: hero.tags.split(',')[1],
    tag3: hero.tags.split(',')[2],
  };
};
