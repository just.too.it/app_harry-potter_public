export interface HeroFromForm {
  name: string;
  gender: string;
  race: string;
  side: string;
  description: string;
  tags: string;
  colorName: string;
  colorBackground: string;
  colorPropertys: string;
  img: string;
}
