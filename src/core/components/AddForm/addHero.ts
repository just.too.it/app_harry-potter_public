import { heroModelFactory } from '../../hero.factory';
import { postHero } from '../../services/hero/hero.service';

import { HeroFromForm } from './heroFromForm.type';

export const addHero = (hero: HeroFromForm): void => {
  void postHero(heroModelFactory(hero));
};
