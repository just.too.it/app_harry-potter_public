import React, { useEffect } from 'react';

import { useHistory } from 'react-router-dom';

import { Button } from '../../../components/Button';
import { IButton } from '../../../components/Button/button.type';
import { Input } from '../../../components/Input';
import { Selector } from '../../../components/Selector';
import { heroesActions } from '../../../store/heroes/heroesSlice';
import { useAppDispatch, useAppSelector } from '../../../store/store.hooks';
import { HEROES_PAGE_ADD } from '../../constants/links';
import { heroFactory } from '../../hero.factory';
import { getFilters } from '../../services/filtering/filtering.service';
import { HeroPreviewModel } from '../../services/hero/heroPreview.model';

export const Filtering = (): React.ReactElement => {
  const router = useHistory();
  const dispatch = useAppDispatch();
  const heroesState = useAppSelector((state) => state.heroesReducer);

  const btnAdd: IButton = {
    type: 'button',
    className: ' heroes__button',
    btnOnClick: () => {
      dispatch(heroesActions.setIsFormOpen(true));
      router.push(HEROES_PAGE_ADD);
    },
  };

  useEffect(() => {
    void getFilters(
      heroesState.query,
      heroesState.filtersByGender,
      heroesState.filtersByRace,
      heroesState.filtersBySide
    )
      .then((data) => (data as HeroPreviewModel[]).map((i) => heroFactory(i)))
      .then((data) => {
        dispatch(heroesActions.setRegistry(data));
      });
  }, [heroesState.query, heroesState.filtersByGender, heroesState.filtersByRace, heroesState.filtersBySide]);

  return (
    <fieldset className="heroes__selection">
      <Input
        type="text"
        name="search"
        className="heroes__search"
        placeholder="Поиск"
        value={heroesState.query}
        onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
          dispatch(heroesActions.setQuery(e.target.value));
        }}
      />
      <div className="heroes__filters">
        <Selector header={'Пол'} options={heroesState.optionsForGender} selectorType={heroesState.filtersByGender} />
        <Selector header={'Раса'} options={heroesState.optionsForRace} selectorType={heroesState.filtersByRace} />
        <Selector header={'Сторона'} options={heroesState.optionsForSide} selectorType={heroesState.filtersBySide} />
      </div>
      <Button btn={btnAdd}>Добавить</Button>
    </fieldset>
  );
};
