import React, { FC } from 'react';

import { Modal } from '../../../components/Modal/index';
import { heroesActions } from '../../../store/heroes/heroesSlice';
import { useAppSelector } from '../../../store/store.hooks';
import { HeroFull } from '../Hero/HeroFull';
import { Hero } from '../Hero/hero.type';

export const ModalWithHero: FC<{ hero: Hero }> = (props) => {
  const heroesState = useAppSelector((state) => state.heroesReducer);

  return (
    <Modal active={heroesState.isHeroModalOpen} setActive={heroesActions.setIsHeroModalOpen}>
      <HeroFull hero={props.hero} />
    </Modal>
  );
};
