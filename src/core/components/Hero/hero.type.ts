import { Gender } from '../../utils/enum/gender.type';
import { Race } from '../../utils/enum/race.type';
import { Side } from '../../utils/enum/side.type';

export interface Hero {
  id: string;
  name: string;
  surname: string;
  gender: {
    id: string;
    value: Gender | string;
  };
  side: {
    id: string;
    value: Side | string;
  };
  race: {
    id: string;
    value: Race | string;
  };
  colorName: string;
  colorBackground: string;
  colorPropertys: string;
  img: string;
  imgModal?: string;
  text: string;
  tag1: string;
  tag2: string;
  tag3: string;
}
