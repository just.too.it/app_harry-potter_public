import React, { FC } from 'react';
import { useHistory } from 'react-router-dom';

import { heroesActions } from '../../../../store/heroes/heroesSlice';
import { useAppDispatch } from '../../../../store/store.hooks';

import { heroFullFactory } from '../../../hero.factory';
import { getHero } from '../../../services/hero/hero.service';
import { HeroFullModel } from '../../../services/hero/heroFull.model';

import { Hero } from '../hero.type';

export const HeroPreview: FC<{ hero: Hero }> = (props) => {
  const { id, name, surname, gender, side, race, colorName, colorBackground, colorPropertys, img } = props.hero;

  const dispatch = useAppDispatch();

  const propertysStyle = {
    background: `${colorBackground}`,
    color: `${colorPropertys}`,
    display: 'block',
  };
  const imgStyle = {
    display: 'block',
  };
  if (img === '') {
    imgStyle.display = 'none';
  }

  if (gender.value === '') {
    propertysStyle.display = 'none';
  }

  const nameStyle = {
    color: `${colorName}`,
  };

  const router = useHistory();

  const handlerModal = () => {
    router.push(`/heroes/${id}`);
    dispatch(heroesActions.setIsHeroModalOpen(true));
    void getHero(props.hero.id)
      .then((data) => heroFullFactory(data as HeroFullModel))
      .then((data) => dispatch(heroesActions.setDisplayHero(data)));
  };

  return (
    <div className="hero" onClick={handlerModal}>
      <div className="hero__wrapper-img">
        <img src={img} className="hero__img" style={imgStyle} />
      </div>
      <div className="hero__name" style={nameStyle}>
        {name}
        <br />
        {surname}
      </div>
      <dl className="hero__propertys" style={propertysStyle}>
        <div className="hero__property">
          <dt>Пол</dt>
          <dd>{gender.value}</dd>
        </div>
        <div className="hero__property">
          <dt>Раса</dt>
          <dd>{race.value}</dd>
        </div>
        <div className="hero__property">
          <dt>Сторона</dt>
          <dd>{side.value}</dd>
        </div>
      </dl>
    </div>
  );
};
