import React, { FC } from 'react';

import { Hero } from '../hero.type';

export const HeroFull: FC<{ hero: Hero }> = (props) => {
  const {
    name,
    surname,
    gender,
    side,
    race,
    colorName,
    colorBackground,
    colorPropertys,
    img,
    text,
    imgModal,
    tag1,
    tag2,
    tag3,
  } = props.hero;

  const nameStyle = {
    color: `${colorName}`,
  };
  const temperamentsStyle = {
    display: 'none',
  };

  const propertysStyle = {
    background: `${colorBackground}`,
    color: `${colorPropertys}`,
    display: 'block',
  };
  const infoStyle = {
    background: '#252020',
  };

  if (gender.value === '') {
    propertysStyle.display = 'none';
  }
  if (name === '') {
    infoStyle.background = '$input-color';
  }

  if (tag1) {
    temperamentsStyle.display = 'flex';
  }

  const imgStyle = {
    display: 'block',
  };

  if (img === '') {
    imgStyle.display = 'none';
  }

  return (
    <div className="hero-full">
      <div className="hero-full__info" style={infoStyle}>
        <div className="hero-full__name" style={nameStyle}>
          {name} {surname}
        </div>
        <dl className="hero-full__propertys" style={propertysStyle}>
          <div className="hero-full__property">
            <dt>Пол</dt> {gender.value !== null ? <dd>{gender.value}</dd> : null}
          </div>
          <div className="hero-full__property">
            <dt>Раса</dt>
            {race.value !== null ? <dd>{race.value}</dd> : null}
          </div>
          <div className="hero-full__property">
            <dt>Сторона</dt>
            {side.value !== null ? <dd>{side.value}</dd> : null}
          </div>
        </dl>
        <p className="hero-full__text">{text || ''}</p>
      </div>
      <div className="hero-full__picture">
        <div className="hero-full__img">
          <img src={imgModal || img} className="hero-full__img" style={imgStyle} />
        </div>
        <ul className="hero-full__temperaments" style={temperamentsStyle}>
          <li className="hero-full__temperament">{tag1}</li>
          <li className="hero-full__temperament">{tag2}</li>
          <li className="hero-full__temperament">{tag3}</li>
        </ul>
      </div>
    </div>
  );
};
