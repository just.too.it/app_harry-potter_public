import React, { FC } from 'react';

import { Link } from 'react-router-dom';

import { Nav } from '../../../components/Nav';
import { Logo } from '../../../components/Logo';
import { navLinks } from '../../utils/navLinks';

export const Header = (): React.ReactElement => {
  return (
    <header className="header">
      <div className="header__logo">
        <div className="logo">
          <Link to="/" title="Harry Potter">
            <Logo />
          </Link>
        </div>
      </div>
      <Nav items={navLinks} />
    </header>
  );
};
