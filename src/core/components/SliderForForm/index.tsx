import React, { useState, FC } from 'react';

import { ButtonIcon } from '../../../components/ButtonIcon';
import { IButtonIcon } from '../../../components/ButtonIcon/buttonIcon.type';
import { Input } from '../../../components/Input';
import { HeroFull } from '../Hero/HeroFull';
import { HeroPreview } from '../Hero/HeroPreview';
import { Hero } from '../Hero/hero.type';

export const SliderForForm: FC<{ hero: Hero }> = (props) => {
  const [slide, setSlide] = useState('slider__view_1');

  const btnPrev: IButtonIcon = {
    type: 'button',
    id: 'prev',
    className: 'pagination__button pagination__button_previous',
    btnOnClick: () => {
      if (slide === 'slider__view_2') {
        setSlide('slider__view_1');
      }
    },
  };

  const btnNext: IButtonIcon = {
    type: 'button',
    id: 'next',
    className: 'pagination__button pagination__button_next',
    btnOnClick: () => {
      if (slide === 'slider__view_1') {
        setSlide('slider__view_2');
      }
    },
  };

  return (
    <div className="slider">
      <p>Предварительный просмотр</p>
      <div className="slider__headers">
        <p className={slide === 'slider__view_1' ? 'slider__header slider__header_active' : 'slider__header'}>Вид 1</p>
        <p className={slide === 'slider__view_2' ? 'slider__header slider__header_active' : 'slider__header'}>Вид 2</p>
      </div>
      <div
        className={
          slide === 'slider__view_1' ? 'slider__view slider__view_1 slider__view_active' : 'slider__view slider__view_1'
        }
      >
        <HeroFull hero={props.hero} />
      </div>
      <div
        className={
          slide === 'slider__view_2' ? 'slider__view slider__view_2 slider__view_active' : 'slider__view slider__view_2'
        }
      >
        <HeroPreview hero={props.hero} />
      </div>
      <ul className="pagination">
        <li>
          <ButtonIcon btn={btnPrev} />
        </li>
        <li>
          <ul className="pagination__list">
            <li>
              <Input
                type="radio"
                id="1"
                className={
                  slide === 'slider__view_1' ? 'pagination__item pagination__item_current' : 'pagination__item'
                }
                onChange={() => setSlide('slider__view_1')}
              />
            </li>
            <li>
              <Input
                type="radio"
                id="2"
                className={
                  slide === 'slider__view_2' ? 'pagination__item pagination__item_current' : 'pagination__item'
                }
                onChange={() => setSlide('slider__view_2')}
              />
            </li>
          </ul>
        </li>
        <li>
          <ButtonIcon btn={btnNext} />
        </li>
      </ul>
    </div>
  );
};
