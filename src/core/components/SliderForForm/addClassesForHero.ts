export const addClassesForHero = () => {
  const slider = document.querySelector('.slider');
  const classNameForHeroPreview = slider?.querySelector('.hero');
  const classNameForHeroFull = slider?.querySelector('.hero-full');
  const classNameForPagination = slider?.querySelector('.pagination');

  classNameForHeroPreview?.classList.add('hero_preview');
  classNameForHeroFull?.classList.add('hero-full_preview');
  classNameForPagination?.classList.add('pagination_preview');
};
